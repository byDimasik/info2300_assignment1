# README

This README shows the instructions how to run the program.

### License

Since this project is totally open-source and anybody can use the code as they want, it uses the MIT license. However, Keep in mind that in addition to free distribution there is no warranty provided.

### Requirements

You must have Python 3 installed on your computer.

### Run

Execute `python3 run.py` in the root folder. 
